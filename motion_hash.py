__author__ = "woolly_sammoth"

from kivy.config import Config
Config.set('graphics', 'width', '1000')
Config.set('graphics', 'height', '800')
Config.set('graphics', 'resizable', '0')
Config.set('graphics', 'fullscreen', '0')
Config.set('input', 'mouse', 'mouse,disable_multitouch')

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from hashlib import new


class MotionHashApp(App):

    def build(self):
        root = BoxLayout(orientation="vertical")
        self.motion_input = TextInput(multiline=True, size_hint_x=1, size_hint_y=0.9, focus=True)
        submit = Button(size_hint_x=1, size_hint_y=0.1, text="Submit")
        submit.bind(on_press=self.hash)
        root.add_widget(self.motion_input)
        root.add_widget(submit)
        return root

    def hash(self, instance):
        text = str(self.motion_input.text)
        self.motion_input.text = new('ripemd160', text).hexdigest()
        return

if __name__ == "__main__":
    MotionHashApp = MotionHashApp()
    MotionHashApp.run()
